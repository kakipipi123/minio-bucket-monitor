import os
import random

from io import BytesIO
from datetime import datetime, timedelta
from minio import Minio

FILE_TYPES = [
    ('FILES', 'vtmetadatazip'),
    ('FILE_BEHAVIORS', 'vt-file-behaviors-bz2'),
    ('URLS', 'vt-urls-bz2')
]


def main():
    minio_endpoint = os.environ.get("MINIO_ENDPOINT", "minio:9000")
    minio_user = os.environ.get("MINIO_USER", "admin")
    minio_password = os.environ.get("MINIO_PASSWORD", "1q2w3e4r")

    minio_client = Minio(endpoint=minio_endpoint, access_key=minio_user, secret_key=minio_password, secure=False)

    print("Creating buckets...")

    for _, bucket in FILE_TYPES:
        if not minio_client.bucket_exists(bucket):
            minio_client.make_bucket(bucket)

            print(f"Created {bucket}")

    start_time = datetime.now() - timedelta(hours=48)
    start_time = start_time.replace(minute=0, second=0, microsecond=0)

    end_time = datetime.now()
    end_time = end_time.replace(minute=0, second=0, microsecond=0)

    current_time = start_time

    while current_time < end_time:
        hour_drop_rate = random.randint(0, 20)

        print(f"Random rate for hour: {hour_drop_rate}%")

        for _ in range(60):
            for file_type, bucket in FILE_TYPES:
                if random.randint(0, 100) > hour_drop_rate:
                    object_name = f'year={current_time.year}/month={current_time.month}' \
                                  f'/day={current_time.day}/hour={current_time.hour}' \
                                  f'/feedFile_{file_type}_{current_time.strftime("%Y_%m_%d_%H_%M")}.bz2'

                    minio_client.put_object(
                        bucket_name=bucket,
                        object_name=object_name,
                        data=BytesIO(os.urandom(32)),
                        length=32
                    )

                    print(f"Finished uploading {object_name}")

            current_time += timedelta(minutes=1)


if __name__ == "__main__":
    main()
